package com.jastarwang.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页控制器
 * 
 * @author Jastar·Wang
 * @email jastar_wang@163.com
 * @site www.jastar-wang.com
 * @date 2018-10-17
 * @since 1.0
 */
@Controller
@RequestMapping("/")
public class IndexController {

	/**
	 * 首页
	 * 
	 */
	@GetMapping("index")
	public String index() {
		return "index";
	}

	/**
	 * 预览页面
	 * 
	 * @param model
	 * @param article md格式的内容
	 */
	@PostMapping("preview")
	public String preview(ModelMap model, String article) {
		System.out.println("报告老板，后台收到了数据：");
		System.out.println(article);

		model.put("article", article);
		return "preview";
	}

}
