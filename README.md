# demo-editormd

#### 项目介绍
本项目简单演示如何在Java web app中使用Editor.md插件的编辑及预览功能，适用于快速入门，更多配置请参考[官方示例](https://pandao.github.io/editor.md/examples/index.html)

#### 软件架构
本项目采用`Spring Boot`+`Thymeleaf`简单架构，并无任何复杂配置，重点在于`Editor.md`的使用


#### 安装教程

1. 点击[这里](https://gitee.com/jastar-wang/demo-editormd.git)进行下载或clone
2. 将本项目导入到IDE中（`Eclipse`或`STS`）
3. 启动`Application.java`文件
4. 浏览器访问：`http://localhost:8080/index`

#### 使用说明

本项目采用`MIT`开源协议，您在保留作者版权的情况下可以自由分享和使用该代码

#### 参与贡献

1. `Fork` 本项目
2. 新建您的分支

	- feature/[author]/[description]
	- hotfix/[author]/[isuse_id]
	
3. 提交代码
4. 新建 `Pull Request`
5. 等待作者 `Merge`
